
[Swift iOS version of this client](https://bitbucket.org/seankang/fcmiosapp/src/master/)


Using Android Studio 4.0

Using Java 1.8

Based on tutorial on
https://www.androidauthority.com/android-push-notifications-with-firebase-cloud-messaging-925075/

https://github.com/firebase/quickstart-android/tree/master/messaging

Upgrading to API 28 to support channel.
https://github.com/android/user-interface-samples/tree/master/Notifications
https://developer.android.com/training/notify-user/channels


To test:

Build and install the app to an android device.

Log into Firebase Console.

Ensure the app package name in the manifest matches the firebase console for Messaging.

Take note of the Server Key :

![APIKEY](/images/firebase-apikey.png "the API KEY to send from app server or postman")

When running the android app, the app will normally send the push notification token to the server so that the server can send notification just to this account.

![TOKEN](/images/pntoken.png "The token linked to this specific device.")

Then the app server can send the notification to the device using this RESTful approach:

![POSTMAN](/images/postman-headers.png "http headers.")

![POSTMAN](/images/postman-send-request.png "Restful POST body request")

This requent to send the notification handles additional notify rich options and can send to one or more devices.
![POSTMAN](/images/postman-bulk-fcm.png "Restful POST body request for rich notify and handling bulk")


Then the message will arrive to the android device:

![ANDROIDBAR](/images/notifybar.png "Android notification bar")



And if the user presses long on the app icon, the notify bubble will display the notification.

![ANDROIDPOPUP](/images/homescreen-notify.png "Android notification bar")
